#!/bin/bash

sudo pacman -Sy \
    base-devel \
    make \
    aria2 \
    vi \
    git \
    htop \
    xorg-xrandr \
    xloadimage \
    xdotool \
    dmenu \
    xsecurelock \
    xscreensaver \
    screenfetch \
    pulseaudio \
    pulseaudio-alsa \
    ibus \
    ibus-hangul \
    adobe-source-han-sans-kr-fonts \
    ttf-baekmuk \
    ttf-dejavu \
    neovim \
    python \
    ripgrep \
    picom \
    lsd \
    bat \
    dunst \
    mplayer \
    gimp \
    mpv \
    firefox-developer-edition \
    google-chrome \
    pulsemixer \
    acpi \
    feh \
    shellcheck \
    alsa-utils \
    rofi \
    the-silver-searcher \
    pavucontrol \
    libinput \
    docker \
    nuget \
    jq \
    dbeaver \
    goldendict \
    pychess \
    anki \
    sqlitebrowser \
    php \
    earlyoom \
    reflector

# Install yay
git clone https://aur.archlinux.org/yay.git
cd yay || echo "Couldn't cd into yay folder"
makepkg -si

cd "$HOME" || echo "No home folder?"

yay -Sy \
    polybar-git \
    nerd-fonts-droid-sans-mono \
    lazygit \
    create_ap-git \
    snapd \
    xst-git \
    yabar-git \
    ibus-bamboo \
    ranger-git \
    neovim-plug \
    nvm \
    nerd-fonts-hack \
    visual-studio-code-bin \
    omnisharp-roslyn \
    ibus-bamboo-git \
    postman \
    mssql-server \
    mssql-tools \
    mssql-cli

ibus restart

VSCODE_EXTENSIONS=(\
    vscodevim.vim \
    waderyan.gitblame \
    ms-dotnettools.csharp \
    angular.ng-template \
    ms-azuretools.vscode-docker \
    eamodio.gitlens \
    mads-hartmann.bash-ide-vscode \
    rogalmic.bash-debug \
    ms-python.python \
    esbenp.prettier-vscode \
    ms-vscode.cpptools \
    formulahendry.code-runner \
    golang.Go \
    sibiraj-s.vscode-scss-formatter \
    yzhang.markdown-all-in-one \
    4ops.terraform \
    dbaeumer.vscode-eslint \
    aaron-bond.better-comments \
    CoenraadS.bracket-pair-colorizer \
    christian-kohler.path-intellisense \
    alefragnani.project-manager \
    dlasagno.wal-theme \
    graphql.vscode-graphql \
    jock.svg
)

for e in "${VSCODE_EXTENSIONS[@]}"; do
    code --install-extension "$e"
done

# Pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

# Add pip to path
echo "\$HOME/.local/bin:\$PATH" >> ~/.bashrc

# . ~/.bashrc

pip install neovim pywal pywalfox

# firefox-pywal integration
pywalfox setup

# Neovim
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash

# Latest NodeJS
nvm install node
node install -g typescript bash-language-server @angular/cli

# Vim Plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
vim +PlugInstall +qall

# Calibre
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

#!/bin/bash

background='#0e181e'
foreground='#cfdeea'
cursor='#cfdeea'

# Colors
color0='#0e181e'
color1='#627894'
color2='#6E849B'
color3='#728DAE'
color4='#7599CB'
color5='#8AA0B7'
color6='#94AFD0'
color7='#cfdeea'
color8='#909ba3'
color9='#627894'
color10='#6E849B'
color11='#728DAE'
color12='#7599CB'
color13='#8AA0B7'
color14='#94AFD0'
color15='#cfdeea'

#!/bin/bash

#notification expire time
NOT_EXP_TIME=500

MESSAGE="Opening scripts folder..."
echo "$MESSAGE"
notify-send "$MESSAGE" -t $NOT_EXP_TIME
bash -c 'cd $HOME/.config/i3/scripts/ ; xst'

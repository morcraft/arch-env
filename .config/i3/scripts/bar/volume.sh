#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/../default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

. "$__dir/../audio_control.sh"

get_icon(){
    [ -z "$1" ] && return 1
    [ $(is_muted) -eq 1 ] && { printf "ﱝ"; return 0; }
    [ "$1" -eq 0 ] && { printf "婢"; return 0; }
    [ "$1" -lt 33 ] && { printf "奄"; return 0; }
    [ "$1" -lt 66 ] && { printf "奔"; return 0; }
    printf "墳"
}

get_color()
{
    [ $(is_muted) -eq 1 ] && printf "!Y BG 0x50${color3:1} Y!"
}

VOLUME="$(get_volume | awk '{print $1}')"
ICON=$(get_icon "$VOLUME")
OUTPUT="$VOLUME%"
COLOR=$(get_color)
printf "%s" "$COLOR $ICON $OUTPUT"

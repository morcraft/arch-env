#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/../default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

[[ -z "$1" ]] && {
    printf "%s" "No display :c"
    exit 1
}

get_workspaces()
{
    i3-msg -t get_workspaces | jq -r 'map(select(.output == "'"$1"'") | if .focused then "<span underline=\"double\" background=\"'"${color2}"'\">  \(.num)  </span>" else "<span underline=\"double\" background=\"'"${color1}"'\">  \(.num)  </span>" end) | join("")'
}

printf "%s" "$(get_workspaces "$1")"

#!/bin/bash

[ -z "$1" ] && ICON="" || ICON="$1"
OUTPUT=$(date +'%a %d %b, %H:%M:%S')
printf "%s" "$ICON $OUTPUT"

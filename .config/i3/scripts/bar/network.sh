#!/bin/bash

[ -z "$1" ] && ICON="" || ICON="$1"
OUTPUT=$(iwgetid -r)
printf "%s" "$ICON $OUTPUT"

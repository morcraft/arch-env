#!/bin/bash

get_active_displays()
{
    xrandr -q | awk '/ connected / {print $1}'
}

export active_displays="$(get_active_displays)"

#!/bin/bash

SCRIPTS="$(find "$HOME/.config/i3/scripts" -type f)"

launch()
{
	VALUE="$(printf "%s" "$SCRIPTS" | rofi -dmenu -theme pywal)"
	[ -n "$VALUE" ] && xst -e bash -i -c "$VALUE >> $HOME/.session_log"
}

launch

#!/bin/bash

CYAN='\033[0;36m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
NC='\033[0m'
NOT_EXP_TIME=2500

[ "$UID" -eq 0 ] || { printf "${YELLOW}%s${NC}\\n" "Creating an access point requires sudo privileges."; exec sudo "$0" "$@"; }

get_devices()
{
	printf "%s" "$(nmcli device show | awk -v _c1=GENERAL.TYPE -v _c2=GENERAL.DEVICE 'BEGIN{printf _c1 "\t" _c2 "\n"} $0 ~ _c1{if (a && a !~ _c1) printf $2 "\t" a "\n"} {a=$2}')"
}

create()
{
	DEVICES=$(get_devices)
	printf "${CYAN}Found devices:${NC}\\n%s\\n\\n" "$DEVICES"
	ETH0=$(printf "%s" "$DEVICES" | awk '$1=="ethernet"{printf $2}')
	[ -z "$ETH0" ] && { echo -e "${RED}No Ethernet interface found. Aborting."; return 1; }
	WLAN0=$(printf "%s" "$DEVICES" | awk '$1=="wifi"{printf $2}')
	[ -z "$WLAN0" ] && { echo -e "${RED}No WiFi interface found. Aborting."; return 1; }
	printf "${YELLOW}WLAN0 is ${CYAN}%s ${YELLOW}and ETH0 is ${CYAN}%s${NC}\\n" "$WLAN0" "$ETH0"
	printf "%s" "Enter hotspot name: "
	read -r AP_NAME
	printf "%s" "Enter hotspot password: "
	read -r -s AP_PW
	MESSAGE="Starting access point..."
	printf "%s" "$MESSAGE"
	notify-send "$MESSAGE" -t "$NOT_EXP_TIME"
	sudo create_ap "$WLAN0" "$ETH0" "$AP_NAME" "$AP_PW"
}

create

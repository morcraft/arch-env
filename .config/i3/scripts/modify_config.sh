#!/bin/bash

CONFIG_FILES=(\
    "$HOME/.config/i3/config.template" \
    "$HOME/.config/nvim/init.vim" \
    "$HOME/.config/dunst/dunstrc.template" \
    "$HOME/.config/qutebrowser/config.py" \
    "$HOME/.config/ranger/rc.conf" \
    "$HOME/.config/rofi/config.rasi" \
    "$HOME/.config/rofi/pywal.rasi.template" \
    "$HOME/.config/yabar/yabar.conf.template" \
    "$HOME/.config/polybar/config" \
    "$HOME/.config/picom/picom.conf"
)

VALUE="$(printf "%s\n" "${CONFIG_FILES[@]}" | rofi -dmenu -theme pywal -p "Config")"
DIRNAME="$(dirname "$VALUE")"
cd "$DIRNAME" || exit
xst -e bash -i -c "nvim $VALUE"

#!/bin/sh

# shellcheck source=/home/christian/.cache/wal/colors.sh
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

env \
	XSECURELOCK_AUTH_BACKGROUND_COLOR="${background}" \
	XSECURELOCK_AUTH_FOREGROUND_COLOR="${foreground}" \
    XSECURELOCK_FONT=Monospace-10 \
	XSECURELOCK_PASSWORD_PROMPT="cursor" \
	XSECURELOCK_SAVER=$HOME/.config/i3/scripts/screensaver.sh \
	XSECURELOCK_SAVER=saver_xscreensaver \
	XSECURELOCK_SHOW_DATETIME=1 \
	xsecurelock

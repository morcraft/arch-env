#!/bin/bash

if [[ -z "$1" ]]; then
    wal -R
else
    if [[ ! -f "$1" ]]; then
        printf "%s" "Invalid path "$1" to set wal picture."
    else
        wal -i "$1"
    fi
fi

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"
. "$__dir/toggle_process.sh" ""

replace_vars()
{
    FILE_PATH="$(printf "%s" "$1" | awk -F: '{print $1}')"
    Z="0" && [[ "$1" == *":"* ]] && Z="$(printf "%s" "$1" | awk -F: '{print $NF}')"

    [[ ! -f "$FILE_PATH" ]] && return 1;

    sed \
        -e "s/\${background}/${background:$Z}/g" \
        -e "s/\${foreground}/${foreground:$Z}/g" \
        -e "s/\${cursor}/${cursor:$Z}/g" \
        -e "s/\${color0}/${color0:$Z}/g" \
        -e "s/\${color1}/${color1:$Z}/g" \
        -e "s/\${color2}/${color2:$Z}/g" \
        -e "s/\${color3}/${color3:$Z}/g" \
        -e "s/\${color4}/${color4:$Z}/g" \
        -e "s/\${color5}/${color5:$Z}/g" \
        -e "s/\${color6}/${color6:$Z}/g" \
        -e "s/\${color7}/${color7:$Z}/g" \
        -e "s/\${color8}/${color8:$Z}/g" \
        -e "s/\${color9}/${color9:$Z}/g" \
        -e "s/\${color10}/${color10:$Z}/g" \
        -e "s/\${color11}/${color11:$Z}/g" \
        -e "s/\${color12}/${color12:$Z}/g" \
        -e "s/\${color13}/${color13:$Z}/g" \
        -e "s/\${color14}/${color14:$Z}/g" \
        -e "s/\${color15}/${color15:$Z}/g" \
        "$FILE_PATH"
}

# Use :N for string deletion after replacement
CONFIG_FILES=(\
    "$HOME"'/.config/yabar/yabar.conf.template:1' \
    "$HOME"'/.config/dunst/dunstrc.template' \
    "$HOME"'/.config/i3/config.template' \
    "$HOME"'/.config/rofi/pywal.rasi.template' \
)

for s in "${CONFIG_FILES[@]}"
do
    FILE_PATH="$(printf "%s" "${s%.template*}")"
    printf "%s\n" "Writing to $FILE_PATH"
    printf "%s" "$(replace_vars "$s")" > "$FILE_PATH"
done

"$__dir"/set_screensaver.sh
# "$__dir"/write_yabar_config_files.sh

i3-msg reload
# "$__dir"/toggle_polybar.sh
toggle_process "dunst" 1
notify-send "Reloaded theme"

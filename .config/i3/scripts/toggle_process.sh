#!/bin/bash

toggle_process() {
	if [ -z $1 ]
	then
		printf "%s\n" "Invalid process to toggle."
		return 1
	fi

	local PID="$(ps -e | grep $1 | awk '{print $1}')"
	local RUNNING=0
	for line in $PID ; do
		kill $line
		RUNNING=1
	done

	if [ "$RUNNING" -eq 0 ] || [ "$2" ]
	then
		nohup "$1" > /dev/null &
        disown
	fi
}

[ ! -z "$1" ] && toggle_process "$1"

#!/bin/bash
NOT_EXP_TIME=1000
MESSAGE="Opening qutebrowser configuration..."
echo "$MESSAGE"
notify-send "$MESSAGE" -t $NOT_EXP_TIME
st -e nvim $HOME/.config/qutebrowser/config.py

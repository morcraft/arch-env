#!/bin/bash

#notification expire time
NOT_EXP_TIME=1000

MESSAGE="Opening ranger configuration..."
echo "$MESSAGE"
notify-send "$MESSAGE" -t $NOT_EXP_TIME
st -e nvim $HOME/.config/ranger/rc.conf

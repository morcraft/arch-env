#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

#notification expire time
NOT_EXP_TIME=1000

print_progress(){
	[ -z "$1" ] && { printf "%s" "Invalid progress. Expected a string"; return 1; }
	[ -z "$2" ] && BAR_WIDTH=25 || BAR_WIDTH="$2"
	printf "%s" "$(source "${__dir}/get_progress_string.sh" "$BAR_WIDTH" "█" "░" "$1")"
}

restart_pulseaudio(){
    killall pulseaudio
    pulseaudio -D &
}

get_volume(){
	#	echo $(awk -F"[][]" '/dB/ { print $2 }' <(amixer sget Master))
	printf "%s" "$(pulsemixer --get-volume | awk '{printf $1}')"
}

set_volume(){

	VOLUME="$(get_volume)"
    # daemon might not be running
    [ -z "$VOLUME" ] && restart_pulseaudio

	case $1 in
		"down") [ "$VOLUME" -gt 0 ] && {
						VALUE=$(("$VOLUME" - 5));
						if [ "$VALUE" -lt 0 ]; then VALUE=0; fi
	 	} || VALUE=0 ;;
		"up") [ "$VOLUME" -lt 100 ] && {
						VALUE=$(("$VOLUME" + 5));
						if [ "$VALUE" -gt 100 ]; then VALUE=100; fi
		} || VALUE=100 ;;
		*) printf "%s" "Invalid argument\\n" ;;
	esac

	[ -z "$VALUE" ] && { printf "%s" "Invalid option to set volume to\\n"; return 1; }

	pulsemixer --set-volume $VALUE
	VOLUME=$(get_volume)

	TITLE="Audio Volume $VOLUME"
	MESSAGE="$(print_progress "$VOLUME")"
	printf "%s\\n%s\\n" "$TITLE" "$MESSAGE"
	notify-send "$TITLE" "$MESSAGE" -t $NOT_EXP_TIME
}

is_muted(){
    pulsemixer --get-mute
}

toggle_audio(){
	pulsemixer --toggle-mute
	# strip out the "muted" value
	MUTED="$(is_muted)"
	STATUS="$([ "$MUTED" = "0" ] && printf "%s" "ON" || printf "%s" "OFF")"
	MESSAGE="Audio toggled $STATUS"
	printf "%s\\n" "$MESSAGE"
	notify-send "$MESSAGE" -t $NOT_EXP_TIME
}

case $1
	in
	toggle) printf "%s\\n" "$(toggle_audio)" ;;
	up) printf "%s\\n" "$(set_volume "up")" ;;
	down) printf "%s\\n" "$(set_volume "down")" ;;
	get-volume) printf "%s\\n" "$(get_volume)" ;;
	get-volume-bar) printf "%s\\n" "$(print_progress "$(get_volume)")" ;;
	print-progress) printf "%s\\n" "$(print_progress "$2" "$3")" ;;
esac

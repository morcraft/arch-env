#!/bin/bash

#notification expire time
NOT_EXP_TIME=500

MESSAGE="Opening nmtui..."
printf "%s\\n" "$MESSAGE"
notify-send "$MESSAGE" -t $NOT_EXP_TIME
# urxvt -e ranger
xst -e bash -i -c 'nmtui'

#!/bin/bash

#notification expire time
NOT_EXP_TIME=1500
EXTENSIONS="\.jpeg|\.jpg|\.png"
WALLPAPERS_PATH="$HOME/Pictures/Wallpapers"

get_wallpapers(){
    ls "$WALLPAPERS_PATH/*" | grep -e "$EXTENSIONS"
}

switch_background()
{
	WALLPAPERS="$(get_wallpapers)"
	MESSAGE="Set background to: ${WALLPAPERS[$1]}"
	eval "feh --bg-center --bg-fill ${WALLPAPERS[$1]}"
	printf "%s" "$MESSAGE"
	notify-send "$MESSAGE" -t "$NOT_EXP_TIME"
}

declare MESSAGE

if [ -z "$1" ]
then
	MESSAGE="Invalid background image target"
else
	switch_background "$1"
fi

if [ -n "$MESSAGE" ]
then (
	printf "%s" "$MESSAGE"
	notify-send "$MESSAGE" -t "$NOT_EXP_TIME"
)
fi

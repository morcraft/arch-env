#!/bin/bash

launch()
{
    [[ ! -f "$HOME/.clips" ]] && return 1;
	VALUE="$(rofi -dmenu -theme pywal -c < "$HOME/.clips")"
	printf "%s" "$VALUE" | xclip -selection clipboard
	xdotool type "$VALUE"
}

launch

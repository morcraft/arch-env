#!/bin/bash

#notification expire time
NOT_EXP_TIME=500
TERMINAL="xst"
launch()
{
	local MESSAGE="Opening $1..."
	eval "$TERMINAL -e $1 &"
	notify-send "$MESSAGE" -t $NOT_EXP_TIME
}

if [ -z "$1" ]
then
	echo "Invalid process to launch. Aborting."
else
	launch "$1"
fi

#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

yabar_conf="$HOME/.config/yabar/yabar.conf"

[[ ! -f "$yabar_conf" ]] && {
    printf "%s" "No yabar config file found"
    exit 1
}

. "$__dir/active_displays.sh"

[[ -z "$active_displays" ]] && exit

for display in $active_displays; do
    sed -e "s/\${display}/${display}/g" "$yabar_conf" > "${yabar_conf}.${display}"
done

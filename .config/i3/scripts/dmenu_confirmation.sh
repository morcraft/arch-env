#!/bin/bash

[ -z "$1" ] && echo "Invalid command to execute. Aborting." && exit 1
[ -z "$2" ] && MESSAGE="$1?" || MESSAGE="$2"

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

NB=${background} #normal background
NF=${color1} #normal foreground
SB=${color0} #selected background
SF=${foreground} #selected foreground
FN="Hack Nerd Font Mono-14"

launch()
{
	CMD='dmenu -nb "$NB" -nf "$NF" -sb "$SB" -sf "$SF" -fn "$FN" -p "$2"'
	VALUE=$(printf "Nope\nSure" | eval "$CMD")
	[ "$VALUE" = "Sure" ] && eval "$1"
}

launch "$1" "$MESSAGE"

#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# flip screens
xrandr --output HDMI-1 --primary --right-of DP-1

wal -R

# set desktop background image

# disable all screen/energy saving
# xset s off
# xset -dpms
# xset s noblank

# start pulseaudio daemon
# pulseaudio -D &

# languages daemon
ibus-daemon -drx
picom &
variety &

"$__dir/toggle_polybar.sh"

# run only if on laptop
# [ -d /sys/module/battery ] && \
# st -e $HOME/.config/i3/scripts/sudo_startup.sh
# exit 1

#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
DEFAULT_COLORS="$__dir/default_colors.sh"
WAL_COLORS="$HOME/.cache/wal/colors.sh"
[ -f "$WAL_COLORS" ] && . "$WAL_COLORS" || . "$DEFAULT_COLORS"

NB=${background} #normal background
NF=${color1} #normal foreground
SB=${color0} #selected background
SF=${foreground} #selected foreground
FN="Hack Nerd Font Mono-12"

launch()
{
    rofi -modi window,drun,run -show drun -sidebar-mode -terminal termite -theme pywal -show-icons
}

launch "$1" "$MESSAGE"

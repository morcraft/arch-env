#!/bin/bash

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

polybar_pid="$(pgrep -x polybar)"

[[ -n "$polybar_pid" ]] && {
    for line in $polybar_pid ; do
        kill "$line" && printf "%s" "Killed $PID" >> ~/.polybar_toggle
    done
    exit
}

. "$__dir/active_displays.sh"

polybar_conf="$HOME/.config/polybar/config"

[[ ! -f "$polybar_conf" ]] && {
    printf "%s\n" "No base config file was found at $polybar_conf" >&2
    exit 1
}

for display in $active_displays; do
    MONITOR=$display polybar --reload default &
done

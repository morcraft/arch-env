#!/bin/bash
NOT_EXP_TIME=1000
MESSAGE="Opening nvim configuration..."
echo "$MESSAGE"
notify-send "$MESSAGE" -t $NOT_EXP_TIME
st -e nvim $HOME/.config/nvim/init.vim
